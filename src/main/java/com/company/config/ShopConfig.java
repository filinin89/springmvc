package com.company.config;

import com.company.interceptors.AdminInterceptor;
import com.company.interceptors.AuthInterceptor;
import com.company.interceptors.CommonUserInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

@Configuration
@Import({})
public class ShopConfig implements WebMvcConfigurer{

    @Autowired
    private AuthInterceptor authInterceptor;
    @Autowired
    private AdminInterceptor adminInterceptor;
    @Autowired
    private CommonUserInterceptor commonUserInterceptor;



    @Override
    public void addInterceptors(InterceptorRegistry registry){
        registry.addInterceptor(authInterceptor)
        .addPathPatterns("/**", "/", "")
        .excludePathPatterns("/auth", "/register", "/logout" ,"/css/**", "/js/**");
        registry.addInterceptor(adminInterceptor)
                .addPathPatterns("/main_admin", "/main_admin/**");
        registry.addInterceptor(commonUserInterceptor)
                .addPathPatterns("/main", "/main/**");

    }

    /*@Bean
    public ViewResolver getViewResolver() {
        InternalResourceViewResolver resolver = new InternalResourceViewResolver();
        resolver.setPrefix("/WEB-INF/pages/");
        resolver.setSuffix(".jsp");
        return resolver;
    }

    @Override
    public void configureDefaultServletHandling(
            DefaultServletHandlerConfigurer configurer) {
        configurer.enable();
    }*/
}
