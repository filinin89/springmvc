package com.company.repository;

import com.company.enums.Role;
import com.company.dto.User;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;

@Repository
public class UsersRepository {

    Map<String, User> usersMap = new HashMap<>(); // можно и список сделать

    private int counter=0;

    public void initiateUsersRepo(){
        User admin = new User("admin", "admin", "admin", "admin", Role.ADMINISTRATOR);
        usersMap.put(admin.getLogin(), admin);
    }

    public void addUser(User user){
        usersMap.put(user.getLogin(), user);
    }

    public Map<String, User> getUsersMap() {
        return usersMap;
    }







}
