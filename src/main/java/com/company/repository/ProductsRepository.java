package com.company.repository;


import com.company.dto.Product;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Random;

@Repository
public class ProductsRepository {

    private String images[] = {"img/no-image-found.jpg","img/no-image-found.jpg", "img/no-image-found.jpg", "img/no-image-found.jpg", "img/no-image-found.jpg", "img/no-image-found.jpg"};
    private String names[] = {"Product1", "Product2", "Product3", "Product4", "Product5", "Product6", "Product7"};
    private int costs[] = {1243, 676, 9097, 2432, 11434, 8999, 7152};
    private Random random = new Random();
    private final int NUMBER_OF_PRODUCTS = 20;
    private final int NUMBER_OF_SECTIONS = 4;

    private ArrayList<ArrayList<Product>> productSections = new ArrayList<>();
    private ArrayList<Product> foundProducts = new ArrayList<>();

    public ArrayList<ArrayList<Product>> getProductSections() {
        return productSections;
    }

    public ArrayList<Product> getFoundProducts() {
        return foundProducts;
    }

    public int getNUMBER_OF_SECTIONS() {
        return NUMBER_OF_SECTIONS;
    }


    public ArrayList<ArrayList<Product>> generateProducts() {
        int counterProd=0;

        for(int i=0; i<NUMBER_OF_SECTIONS; i++) {

            ArrayList<Product> products = new ArrayList<>();

            for (int j = 0; j < NUMBER_OF_PRODUCTS; j++, counterProd++ ) {
                Product product = new Product();
                product.setName("Product" + counterProd); //names[random.nextInt(names.length)]
                product.setImg(images[random.nextInt(images.length)]);
                product.setCost(costs[random.nextInt(costs.length)]);
                product.setId(counterProd);

                products.add(product);
            }
            productSections.add(products);
        }

        return productSections;
    }

    public void addSection(){
        ArrayList<Product> products = new ArrayList<>();
        productSections.add(products);
    }
}
