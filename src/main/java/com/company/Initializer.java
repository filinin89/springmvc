package com.company;

import com.company.repository.ProductsRepository;
import com.company.repository.UsersRepository;
import com.company.service.ProductsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class Initializer {

    @Autowired
    UsersRepository usersRepository;

    @Autowired
    ProductsService productsService;

    @PostConstruct
    public void init(){
        usersRepository.initiateUsersRepo();
        productsService.generateProducts();
    }
}
