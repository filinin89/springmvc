package com.company.dto;

public class Product {

    private String img;
    private String name;
    private int cost;
    private int id;

    public Product(){

    }

    public Product(String img, String name, int cost, int id) {
        this.img = img;
        this.name = name;
        this.cost = cost;
        this.id = id;
    }

    //private int category;

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    /*public void setCategory(int category) {
        this.category = category;
    }*/

    public String getImg() {
        return img;
    }

    public String getName() {
        return name;
    }

    public int getCost() {
        return cost;
    }



}
