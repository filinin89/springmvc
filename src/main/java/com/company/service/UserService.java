package com.company.service;

import com.company.exceptions.SuchUserExists;
import com.company.exceptions.UserNotFound;
import com.company.exceptions.WrongPasswordException;
import com.company.dto.User;
import com.company.repository.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    @Autowired
    UsersRepository usersRepository;

    public void addUser(User user) throws SuchUserExists {

        if(usersRepository.getUsersMap().get(user.getLogin()) != null){
            throw new SuchUserExists();
        }
        usersRepository.addUser(user);
    }

    public User getEntireUser(String login){
        return usersRepository.getUsersMap().get(login);
    }


    public void checkLoginAndPassword(String login, String password) throws WrongPasswordException, UserNotFound {

        User user = usersRepository.getUsersMap().get(login);
        if(user == null){
            throw new UserNotFound();
        }
        if(!password.equals(user.getPassword())){
            throw new WrongPasswordException();
        }

    }

}
