package com.company.service;

import com.company.dto.Product;
import com.company.repository.ProductsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class ProductsService {

    @Autowired
    private ProductsRepository productsRepository;


    private ArrayList<ArrayList<Product>> productSections;



    public ArrayList<ArrayList<Product>> generateProducts(){

        productSections = productsRepository.generateProducts();
        return productSections;
    }

    public void addProduct(int section, Product product){
        productSections.get(section).add(product);
    }

    public Product getProductById(int id){
        Product product = findProductById(id);
        Product receivedProduct = product;
        productsRepository.addSection();
        addProduct(productsRepository.getNUMBER_OF_SECTIONS(), receivedProduct); // в последней секции будут храниться найденные товары
        return receivedProduct;
    }



    public void updateProduct(String name, Product updateProduct) {
        for(ArrayList<Product> section : productSections){
            for(Product product : section){
                if(product.getName().equals(name)){
                    section.set(section.indexOf(product), updateProduct);
                    return;
                }
            }
        }
    }

    public void deleteProduct(int id){
        for(ArrayList<Product> section : productSections) {
            for (Product product : section) {
                if (product.getId()==id) {
                    section.remove(product);
                    return;
                }
            }
        }
    }


    public Product findProductById(int id){
        for(ArrayList<Product> section : productSections){
            for(Product product : section){
                if(product.getId()==id){
                    return product;
                }
            }
        }
        return null;
    }

    public ArrayList<Product> findProductByName(String wantedProductName){
        for(ArrayList<Product> section : productSections) {
            for (Product product : section) {
                if(wantedProductName.equals(product.getName())){
                    productsRepository.getFoundProducts().add(product);
                    return productsRepository.getFoundProducts();
                }
            }
        }
        return null;
    }

    public ArrayList<Product> findProductByCost(int wantedProductCost){
        for(ArrayList<Product> section : productSections) {
            for (Product product : section) {
                if (wantedProductCost == product.getCost()) {
                    productsRepository.getFoundProducts().add(product);
                    return productsRepository.getFoundProducts();
                }
            }
        }
        return null;
    }





}
