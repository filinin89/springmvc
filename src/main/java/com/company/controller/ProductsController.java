package com.company.controller;

import com.company.dto.Product;
import com.company.repository.ProductsRepository;
import com.company.service.ProductsService;
import com.company.service.SessionUserManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;

@Controller
public class ProductsController {


    @Autowired
    private ProductsService productsService;

    @Autowired
    private ProductsRepository productsRepository;

    @Autowired
    private SessionUserManager userManager;

    //private ModelAndView modelAndView = new ModelAndView();

    @GetMapping({"/main", "/"})
    public ModelAndView loadProducts(HttpSession session, ModelAndView modelAndView) {

        modelAndView.addObject("ProductsSections", productsRepository.getProductSections());
        modelAndView.addObject("sizeProductsSections", productsRepository.getProductSections().size());

        modelAndView.addObject("userRole", session.getAttribute("userRole"));
        modelAndView.addObject("login", session.getAttribute("login")); // положим из сессии в модель
        modelAndView.setViewName("main");
        return modelAndView;
    }

    @GetMapping("/main_admin")
    public ModelAndView loadProductsForAdmin(HttpSession session, ModelAndView modelAndView){

        modelAndView.addObject("ProductsSections", productsRepository.getProductSections());
        modelAndView.addObject("sizeProductsSections", productsRepository.getProductSections().size());

        //modelAndView.addObject("userRole", session.getAttribute("userRole"));
        modelAndView.addObject("login", session.getAttribute("login")); // положим из сессии в модель
        modelAndView.setViewName("main_admin");
        return modelAndView;
    }

    @GetMapping("/logout")
    public void logout(ModelAndView modelAndView, HttpSession session){
        session.invalidate();
        userManager.setCurrentUser(null);
    }

    @PostMapping("/addProduct")
    public ModelAndView addProduct(ModelAndView modelAndView, @ModelAttribute("addedProduct") Product addedProduct, @RequestParam("section") Integer section, HttpSession session){
        if(productsService.findProductByName(addedProduct.getName())!=null){
            modelAndView.setViewName("redirect:/updateProduct"); // если такой продукт есть, то делаем обновление
            session.setAttribute("existingProductName", addedProduct.getName());
            session.setAttribute("existingProduct", addedProduct);
            return modelAndView;
        }
        productsService.addProduct(section, addedProduct);
        modelAndView.addObject("ProductsSections", productsRepository.getProductSections()); // обновим секции
        modelAndView.addObject("sizeProductsSections", productsRepository.getProductSections().size());
        modelAndView.setViewName("main_admin");
        return modelAndView;
    }


    @GetMapping("/updateProduct")
    public String updateProduct(@RequestParam(required=false) Product updateProduct, ModelAndView modelAndView, HttpSession session){
        productsService.updateProduct((String)session.getAttribute("existingProductName"), (Product)session.getAttribute("existingProduct"));
        return "redirect:/main_admin";
    }

    @GetMapping("/getProduct")
    public ModelAndView getProduct(@RequestParam Integer id, ModelAndView modelAndView){
        Product product = productsService.getProductById(id);
        //modelAndView.addObject("ProductsSections", productsRepository.getProductSections());
        modelAndView.setViewName("redirect:/main_admin"); // если редиректим, то товары можно не обновлять
        return modelAndView;
    }


    @PostMapping("/deleteProduct")
    public String deleteProduct(@RequestParam(required=false) Integer id, ModelAndView modelAndView){
        productsService.deleteProduct(id);
        //modelAndView.addObject("ProductsSections", productsRepository.getProductSections()); // обновим секции
        return "redirect:/main_admin";
    }



    @PostMapping("/findByName")
    public ModelAndView findProductByName(@RequestParam("searchProduct") String searchProductName, ModelAndView modelAndView){
        ArrayList<Product> foundProducts = productsService.findProductByName(searchProductName);

        modelAndView.addObject("foundProducts", foundProducts);
        modelAndView.addObject("sizeProductsSections", productsRepository.getProductSections().size());
        modelAndView.setViewName("redirect:/main");
        return modelAndView;
    }


    @PostMapping("/findByCost")
    public ModelAndView findProductByCost(@RequestParam("searchProduct") Integer searchProductCost, ModelAndView modelAndView){
        ArrayList<Product> foundProducts = productsService.findProductByCost(searchProductCost);

        modelAndView.addObject("foundProducts", foundProducts);
        modelAndView.addObject("sizeProductsSections", productsRepository.getProductSections().size());
        modelAndView.setViewName("redirect:/main");
        return modelAndView;
    }
}
